<div id="sliderDiv">
  <div id="foodSlider" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#foodSlider" data-slide-to="0" class="active"></li>
      <li data-target="#foodSlider" data-slide-to="1"></li>
      <li data-target="#foodSlider" data-slide-to="2"></li>
      <li data-target="#foodSlider" data-slide-to="3"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="img/pizza1.jpg" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h5>Pizza 1</h5>
          <p>Ingredientes / Descripcion de la Pizza</p>
        </div>
      </div>
      <div class="carousel-item">
        <img src="img/pizza2.jpg" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h5>Pizza 2</h5>
          <p>Ingredientes / Descripcion de la Pizza</p>
        </div>
      </div>
      <div class="carousel-item">
        <img src="img/pizza3.jpg" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
        <h5>Pizza 3</h5>
          <p>Ingredientes / Descripcion de la Pizza</p>
        </div>
      </div>
      <div class="carousel-item">
        <img src="img/pizza4.jpg" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
        <h5>Pizza 4</h5>
          <p>Ingredientes / Descripcion de la Pizza</p>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#foodSlider" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#foodSlider" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>