<?php
$curl = curl_init();

$params = [ 
    'client_id' => '5c02283e03e74080a93369e9a2f913fe',
    'client_secret' => 'a614cfdd64a54983b8ff721af6b235d9',
    'grant_type' => 'authorization_code',
    'redirect_uri' => 'http://pizzasctes.epizy.com',
    'code' => $code
];

$defaults = array( 
    CURLOPT_URL => 'https://api.instagram.com/oauth/access_token',
    CURLOPT_POST => true,
    CURLOPT_POSTFIELDS => $params,
    CURLOPT_VERBOSE => 0,
    CURLOPT_RETURNTRANSFER => TRUE
);

curl_setopt_array( $curl, $defaults );

if ( $content = curl_exec( $curl ) ) {

    $content = json_decode( $content);

    $userName = $content->user->username;
    $fullName = $content->user->full_name;
    $profileImg = $content->user->profile_picture;
}

curl_close( $curl );